/**
 * Calculates the next step/direction to move in the maze
 *
 * @author Tyler Rodriguez
 */

public class NewStepCalculator {
    private final int rowLength;

    /**
     * NextStepCalculator Constructor
     * 
     * @param rowLength length of each row in the maze
     */
    public NewStepCalculator(int rowLength) {
        this.rowLength = rowLength;
    }

    /**
     * Calculates the new step based on the current character and the step direction
     * 
     * @param current character
     * @param step    representing the direction and magnitude
     * @return integer new step
     */
    public int calculateNewStep(char currentChar, int step) {

        switch (currentChar) {
        case 'v':
            return rowLength;
        case '>':
            return 1;
        case '<':
            return -1;
        case '^':
            return -rowLength;
        case 'O':
            return -step;
        case '/':
            if (step == 1) {
                return -rowLength;
            } else if (step == rowLength) {
                return -1;
            } else if (step == -1) {
                return rowLength;
            } else if (step == -rowLength) {
                return 1;
            }
        case '\\':
            if (step == 1) {
                return rowLength;
            } else if (step == rowLength) {
                return 1;
            } else if (step == -1) {
                return -rowLength;
            } else if (step == -rowLength) {
                return -1;
            }
        }

        return step;
    }
}
