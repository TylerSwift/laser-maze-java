/**
 * Main driver class that evaluates the maze
 *
 * @author Tyler Rodriguez
 */

public class Lasermaze implements Testable {

  /**
   * Instantiates all objects and calls all necessary methods to evaluate maze and
   * find the distance the laser travels
   * 
   * @return integer representing the distance the laser travels or -1 if an
   *         infinite loop is detected
   */
  public int evaluate(String input) {
    InputProcessor inputProcessor = new InputProcessor(input);
    char[] mazeChars = inputProcessor.getArrayOfChars();
    int rowLength = inputProcessor.getArrayOfCharsRowLength();
    int startingPosition = inputProcessor.getStartingPosition('@');

    InfiniteLoopCheckable repeatedSequenceChecker = new RepeatedSequenceChecker();
    NewStepCalculator newStepCalculator = new NewStepCalculator(rowLength);

    LaserMazeSolver laserMazeSolver = new LaserMazeSolver(mazeChars, rowLength, startingPosition,
        repeatedSequenceChecker, newStepCalculator);

    return laserMazeSolver.solveLaserMaze();
  }
}
