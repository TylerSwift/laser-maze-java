
/**
 * Solves the lasermaze by calculating the distance the laser travels
 * 
 * @author Tyler Rodriguez
 */

import java.util.*;

public class LaserMazeSolver {
    private final char[] laserMaze;
    private final int rowLength;
    private final int startingPosition;
    private final InfiniteLoopCheckable infiniteLoopCheckable;
    private final NewStepCalculator newStepCalculator;

    /**
     * LaserMazeSolver Constructor
     * 
     * @param character             array representing the actual laser maze
     * @param integer               representing the length of each row in the maze
     * @param integer               representing the starting position of the laser
     * @param InfiniteLoopCheckable used to check if an infinite loop has been
     *                              reached
     * @param NewStepCalculator     used to find the new step/direction when coming
     *                              across a prism or mirror
     */
    public LaserMazeSolver(char[] laserMaze, int rowLength, int startingPosition,
            InfiniteLoopCheckable infiniteLoopCheckable, NewStepCalculator newStepCalculator) {
        this.laserMaze = laserMaze;
        this.rowLength = rowLength;
        this.startingPosition = startingPosition;
        this.infiniteLoopCheckable = infiniteLoopCheckable;
        this.newStepCalculator = newStepCalculator;
    }

    /**
     * Checks if the laser completed traversing the maze
     * 
     * @param integer represnting the current position in the maze
     * @param integer representing the step size/direction
     * @return true if laser completed the maze
     */
    private boolean checkIfComplete(int current, int step) {
        if (step == rowLength) {
            return (current >= laserMaze.length - rowLength);
        }

        if (step == -rowLength) {
            return (current < rowLength);
        }

        if (step == -1) {
            return (current % rowLength == 0);
        }

        if (step == 1) {
            return (current + 1) % rowLength == 0;
        }

        return false;
    }

    /**
     * calculates the distance the laser travels whilst checking for an infinite
     * loop every time a postion is repeated
     * 
     * @return distance the laser travelled through the maze or -1 if there is an
     *         infinite loop
     */
    public int solveLaserMaze() {
        List<Integer> listOfVisitedPositions = new ArrayList<>();
        Set<Integer> setOfVisitedPositions = new HashSet<>();
        char currentCharacter;
        boolean mazeCompleted = false;
        int step = 1;
        int totalDistance = 1;
        int currentPosition = startingPosition;

        laserMaze[currentPosition] = '-';

        while (!mazeCompleted) {

            if (setOfVisitedPositions.contains(currentPosition)) {
                listOfVisitedPositions.add(currentPosition);
                if (infiniteLoopCheckable.checkForInfiniteLoop(listOfVisitedPositions)) {
                    return -1;
                }
            } else {
                setOfVisitedPositions.add(currentPosition);
                listOfVisitedPositions.add(currentPosition);
            }

            currentPosition += step;
            totalDistance += 1;
            currentCharacter = laserMaze[currentPosition];

            if (currentCharacter != '-') {
                step = newStepCalculator.calculateNewStep(currentCharacter, step);
            }

            mazeCompleted = checkIfComplete(currentPosition, step);
        }

        return totalDistance;
    }
}
