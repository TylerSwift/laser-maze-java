
/**
 * Checks a list of visited maze positions for an inifite loop
 *
 * @author Tyler Rodriguez
 */

import java.util.List;

public interface InfiniteLoopCheckable {

    /**
     * Returns true if it finds an infinite loop from the list of positions visited
     * so far
     *
     * @param list of visited positions
     * @return true if there is an infinite loop detected in the list
     */
    boolean checkForInfiniteLoop(List<Integer> list);
}
