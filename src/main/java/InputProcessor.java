/**
 * Process the input String so that it can be used as a char array
 *
 * @author Tyler Rodriguez
 */

public class InputProcessor {
    private final String input;

    /**
     * InputProcessor Constructor
     * 
     * @param input string representation of the maze
     */
    public InputProcessor(String input) {
        this.input = input;
    }

    /**
     * Converts the maze input string into character array while removing newlines
     * 
     * @return character array without newlines
     */
    public char[] getArrayOfChars() {
        return input.replaceAll("\\n", "").toCharArray();
    }

    /**
     * Calculates the length of the rows in the maze
     * 
     * @return integer specifying the row length
     */
    public int getArrayOfCharsRowLength() {
        return input.split("\\n")[0].length();
    }

    /**
     * Gets the starting position by finding the index of the starting position
     * character
     * 
     * @param starting character (@)
     * @return integer specifying the starting position of the maze
     */
    public int getStartingPosition(int startingCharacter) {
        int index = 0;
        char[] arrayOfChars = getArrayOfChars();

        while (arrayOfChars[index] != startingCharacter) {
            index++;
        }

        return index;
    }
}