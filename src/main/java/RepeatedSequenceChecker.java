
/**
 * Checks for an infinite loop checking if there is a repeated sequence
 * Implements InfiniteLoopCheckable
 *
 * @author Tyler Rodriguez
 */

import java.util.List;

public class RepeatedSequenceChecker implements InfiniteLoopCheckable {

    /**
     * Checks for an infinite loop by checking if there is a repeated sequence of 2
     * integers
     *
     * @param list of visited positions
     * @return true if there a repeated sequence of two integers
     */
    public boolean checkForInfiniteLoop(List<Integer> list) {
        int current = list.get(list.size() - 1);
        int index = list.size() - 2;
        int temp = list.get(index);

        while (index > 1) {
            if (list.get(index - 1) == temp && list.get(index) == current) {
                return true;
            }
            index--;
        }

        return false;
    }
}
